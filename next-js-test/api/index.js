import axios from './axios';
import {X_RAPID_API_HOST} from "../helpers/constant";

export const Api = {
    async query(resource, params) {
        return await axios.get(resource, {
            params: {
                q: params.q,
                units: 'metric',
            },
            headers: {
                "X-RapidAPI-Key": params.apikey,
                "X-RapidAPI-Host": X_RAPID_API_HOST,
            }
        })
    }
}

//Weather
export const WeatherApi = {
    async current(params) {
        return await Api.query('/weather', params)
    },

    async dailyForecast(params) {
        return await Api.query('/forecast/daily', params)
    },

    async search(params) {
        return await Api.query('/find', params)
    },

    async history(params) {
        return await Api.query('/onecall/timemachine', params)
    },

    async month(params) {
        return await Api.query('/climate/month', params)
    },

    async hoursForecast(params) {
        return await Api.query('/forecast', params)
    },
}