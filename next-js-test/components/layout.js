import React, {useState} from "react";
import { Layout, Menu, Button } from 'antd';
import {UserOutlined, VideoCameraOutlined, MenuUnfoldOutlined, MenuFoldOutlined} from '@ant-design/icons';
import Link from "next/link";
import Head from "next/head";
import {logout} from "../helpers/tokenizer"

export default function MainLayout({children, title}) {
    const { Header, Content, Footer, Sider } = Layout;

    const [collapsed, setCollapsed] = useState(false);
    const toggle = () => setCollapsed(!collapsed);

    return (
        <>
            <Head>
                <title>Axles Weather App | {title}</title>
                <meta name="keywords" content="yurii, kniazyk, next, js" />
            </Head>
            <Layout>
                <Sider trigger={null} collapsible collapsed={collapsed} >
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<UserOutlined />}>
                            <Link href={'/current'}><a>Current Weather Data</a></Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                            <Link href={'/daily-forecast'}><a>Call 16 day</a></Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header
                        className="site-layout-background"
                        style={{
                            padding: "0 24px",
                            background: "transparent",
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center"
                        }}
                    >
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}
                        <Button onClick={logout} type="link">Logout</Button>
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                        {children}
                    </Content>
                    <Footer style={{textAlign: 'center'}}>AXLES ©2021 Created by Yurii Kniazyk</Footer>
                </Layout>
            </Layout>
        </>
    )
}