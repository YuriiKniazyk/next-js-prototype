export const X_RAPID_API_HOST = 'community-open-weather-map.p.rapidapi.com';
export const BASE_URL = 'https://community-open-weather-map.p.rapidapi.com';

export const columns = [
    {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
    },
    {
        title: 'Clouds',
        dataIndex: 'clouds',
        key: 'clouds',
    },
    {
        title: 'Humidity',
        dataIndex: 'humidity',
        key: 'humidity',
    },
    {
        title: 'Pop',
        dataIndex: 'pop',
        key: 'pop',
    },
    {
        title: 'Pressure',
        dataIndex: 'pressure',
        key: 'pressure',
    },
    {
        title: 'Rain',
        dataIndex: 'rain',
        key: 'rain',
    },
    {
        title: 'Speed',
        dataIndex: 'speed',
        key: 'speed',
    },
    {
        title: 'Sunrise',
        dataIndex: 'sunrise',
        key: 'sunrise',
    },
    {
        title: 'Sunset',
        dataIndex: 'sunset',
        key: 'sunset',
    },
];