import Router from 'next/router';
import Cookies from 'js-cookie';

export function getAppCookies(req) {
    const parsedItems = {};
    if (req.headers.cookie) {
        const cookiesItems = req.headers.cookie.split('; ');
        cookiesItems.forEach(cookies => {
            const parsedItem = cookies.split('=');
            parsedItems[parsedItem[0]] = decodeURI(parsedItem[1]);
        });
    }
    return parsedItems;
}

export function logout(e) {
    e.preventDefault();
    Cookies.remove('apikey');
    Cookies.remove('city');
    Router.push('/');
};