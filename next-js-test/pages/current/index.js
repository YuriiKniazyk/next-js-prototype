import React, {useEffect} from "react";
import {Result, Card} from "antd";
import Router from 'next/router';

import MainLayout from "../../components/layout";
import {getAppCookies} from "../../helpers/tokenizer";
import {WeatherApi} from "../../api";

export default function Wineries({city, apikey, e, weatherData}) {
    useEffect(() => { if (!city && !apikey) Router.push('/') }, []);
    const {coord, main, name, sys, weather, wind} = weatherData;

    return (
        <MainLayout title={'Current'}>
            {
                !e ? <div className="site-card-border-less-wrapper">
                        <Card title={`Current Weather for ${name} ${sys.country}`} style={{maxWidth: 1000, margin: '0 auto'}}>
                            <h2>Description: {weather[0].description}, {weather[0].main}</h2>

                            <section style={{display: 'flex', justifyContent: 'space-between'}}>
                                <div>
                                    <p>Coordinates: </p>
                                    <ul>
                                        <li>Longitude: {coord.lon}</li>
                                        <li>Latitude: {coord.lat}</li>
                                    </ul>
                                </div>
                                <div>
                                    <p>System</p>
                                    <ul>
                                        <li>Sunrise {new Date(sys.sunrise).toISOString().split('T')[1].split('.')[0]}</li>
                                        <li>Sunset {new Date(sys.sunset).toISOString().split('T')[1].split('.')[0]}</li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Main</p>
                                    <ul>
                                        <li>Feels like: {main.feels_like}</li>
                                        <li>Humidity: {main.humidity}</li>
                                        <li>Pressure: {main.pressure}</li>
                                        <li>Temp: {main.temp}</li>
                                        <li>Temp max: {main.temp_max}</li>
                                        <li>Temp min: {main.temp_min}</li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Wind</p>
                                    <ul>
                                        <li>Deg: {wind.deg}</li>
                                        <li>Speed: {wind.speed}</li>
                                    </ul>
                                </div>
                            </section>
                        </Card>
                    </div> :
                    <Result
                        status="500"
                        title={e}
                    />

            }
        </MainLayout>
    )
}

export async function getServerSideProps({req}) {
    const { city, apikey } = getAppCookies(req);
    let weatherData = {}, error = '';

    const res = await WeatherApi.current({q: city, apikey}).catch(e => error = e.response.data.message);
    if (res && res.status === 200) weatherData = res.data;

    return {
        props: {
            city,
            apikey,
            e: error,
            weatherData
        },
    };
}