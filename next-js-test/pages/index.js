import React, {useState} from "react";
import { Form, Input, Button, Result, Typography } from "antd";
import Router from 'next/router';
import Cookies from 'js-cookie';

import style from "../styles/auth.module.css"

export default function Auth() {
  const { Title } = Typography;
  const [error, setError] = useState('');

  const layout = {
    labelCol: {
      span: 9,
    },
    wrapperCol: {
      span: 5,
    },
  };
  const tailButton = {
    wrapperCol: {
      offset: 12,
      span: 6,
    },
  };

  const onSubmit = async ({apikey, city}) => {
      await Cookies.set('apikey', apikey);
      await Cookies.set('city', city);
      await Router.push('/current');
  };

  return (
    <>
      <div className={style.container}>
        <Title level={2} style={{textAlign: 'right', paddingRight: '20px'}}>AXLES Weather App</Title>

        <Form
            {...layout}
            name="nest-messages"
            onFinish={onSubmit}
        >
          <Form.Item
              name="apikey"
              label="Rapid API Key"
              rules={[
                {
                  required: true,
                  message: 'Please input your Rapid API Key!'
                },
              ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
              name="city"
              label="City"
              rules={[
                {
                  required: true,
                  message: 'Please input your City!'
                },
              ]}
          >
            <Input />
          </Form.Item>

          <Form.Item {...tailButton}>
            <Button
                type="primary"
                htmlType="submit"
            >
              Submit
            </Button>
            <Button type={'link'} href='https://rapidapi.com/community/api/open-weather-map' target={'_blank'}>Create Api Key</Button>
          </Form.Item>
        </Form>

        {
          error.length > 0 && (
              <Result
                  status="500"
                  title={error}
              />
          )
        }
      </div>
    </>
  )
}