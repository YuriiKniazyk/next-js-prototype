import React, {useEffect} from "react";
import {Result, Card, Table} from "antd";
import Router from 'next/router';

import MainLayout from "../../components/layout";
import {getAppCookies} from "../../helpers/tokenizer";
import {WeatherApi} from "../../api";
import {columns} from "../../helpers/constant";

export default function Wineries({cityInput, apikey, e, weatherData}) {
    useEffect(() => { if (!cityInput && !apikey) Router.push('/') }, []);
    const {list, city} = weatherData;

    const dataSource = list.map(item => {
        return {
            key: item.dt,
            date: new Date(item.dt).toISOString().split('T')[0].split('.')[0],
            clouds: 32,
            humidity: item.humidity,
            pop: item.pop,
            pressure: item.pressure,
            rain: item.rain,
            speed: item.speed,
            sunrise: new Date(item.sunrise).toISOString().split('T')[1].split('.')[0],
            sunset: new Date(item.sunset).toISOString().split('T')[1].split('.')[0],
            children: [
                {
                    feels_like: item.feels_like,
                    temp: item.temp,
                    weather: item.weather[0]
                }
            ]
        }
    });

    return (
        <MainLayout title={'Current'}>
            {
                !e ?
                    <div className="site-card-border-less-wrapper">
                        <Card title={`Current Weather for ${city.name} ${city.country}`} style={{maxWidth: 1000, margin: '0 auto'}}>
                            <section style={{display: 'flex', justifyContent: 'space-between', maxWidth: 500}}>
                                <div>
                                    <p>Coordinates: </p>
                                    <ul>
                                        <li>Longitude: {city.coord.lon}</li>
                                        <li>Latitude: {city.coord.lat}</li>
                                    </ul>
                                </div>
                                <div>
                                    <p>System</p>
                                    <ul>
                                        <li>Population: {city.population}</li>
                                        <li>Timezone: {city.timezone}</li>
                                    </ul>
                                </div>
                            </section>
                        </Card>
                        <Table dataSource={dataSource} columns={columns}
                            expandable={{
                                expandedRowRender: record => {
                                    return record.children.map(child => {
                                        return (
                                            <div style={{display: 'flex', justifyContent: 'space-between', maxWidth: 800, margin: '0 auto'}}>
                                                <div>
                                                    <h3>Feels like: </h3>
                                                    <ul>
                                                        <li>Day: {child.feels_like.day}</li>
                                                        <li>Night: {child.feels_like.night}</li>
                                                        <li>Evening: {child.feels_like.eve}</li>
                                                        <li>Morning: {child.feels_like.morn}</li>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h3>Temp: </h3>
                                                    <ul>
                                                        <li>Day: {child.temp.day}</li>
                                                        <li>Night: {child.temp.night}</li>
                                                        <li>Evening: {child.temp.eve}</li>
                                                        <li>Morning: {child.temp.morn}</li>
                                                        <li>Max: {child.temp.max}</li>
                                                        <li>Min: {child.temp.min}</li>
                                                    </ul>
                                                </div>
                                                <div>
                                                    <h3>Weather: </h3>
                                                    <ul>
                                                        <li>Main: {child.weather.main}</li>
                                                        <li>Description: {child.weather.description}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            }}
                        />
                    </div> :
                        <Result
                            status="500"
                            title={e}
                        />

            }
        </MainLayout>
    )
}

export async function getServerSideProps({req}) {
    const { city, apikey } = getAppCookies(req);
    let weatherData = {}, error = '';

    const res = await WeatherApi.dailyForecast({q: city, apikey}).catch(e => error = e.response.data.message);
    if (res && res.status === 200) weatherData = res.data;

    return {
        props: {
            cityInput: city,
            apikey,
            e: error,
            weatherData
        },
    };
}