import NextNprogress from 'nextjs-progressbar';

import 'antd/dist/antd.css';
import '../styles/globals.css'
import Head from "next/head";

export default function MyApp({ Component, pageProps }) {
  return (
      <>
          <Head>
              <title>Axles Weather App</title>
              <meta name="keywords" content="axles, yurii, kniazyk, next, js, weather" />
          </Head>
          <NextNprogress
              color="#29D"
              startPosition={0.3}
              stopDelayMs={200}
              height="5"
          />
          <Component {...pageProps} />
      </>
  )
}